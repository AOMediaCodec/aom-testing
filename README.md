# Testing Resources for the AOMedia Codec Projects

This project contains the CI testing resources and scripts used for additional testing by the AOMedia Codec projects

## License

This project is is licensed under the Alliance for Open Media license and Patent License. See [LICENSE](LICENSE.md) and [PATENTS](PATENTS.md) for details.

## How to Contribute

We welcome community contributions. Thank you for your time! By contributing to the project, you agree to the license, patent and copyright terms in the AOM License and Patent License  and to the release of your contribution under these terms. See [LICENSE](LICENSE.md) and [PATENTS](PATENTS.md) for details.

## Contributor agreement

You will be required to execute the appropriate [contributor agreement](http://aomedia.org/license/) to ensure that the AOMedia Project has the right to distribute your changes.

### Contribution process

- Submit a pull request for review to the maintainer

### How to Report Bugs and Provide Feedback

Use the [Issues](https://gitlab.com/AOMediaCodec/aom-testing/issues) tab on Github. To avoid duplicate issues, please make sure you go through the existing issues before logging a new one.
