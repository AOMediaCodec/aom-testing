import multiprocessing, subprocess
import os, sys, shutil, re
import time

#CPU_COUNT = multiprocessing.cpu_count() #num pool for lp

CPU_COUNT = int(multiprocessing.cpu_count() // 5) # num pool for non-lp


presets = []

r2r_commands = [
'./SvtAv1EncApp --preset 10 --passes 1 --input-depth 10 -n 60 -w 1920 -h 1080 --fps-num 50000 --fps-denom 1000 -q 62 --keyint 52 --enable-tpl-la 1 -i /home/inteladmin/stream/weekend_run_set/10bit/ducks_take_off_1080p50_60f_10bit.yuv -b bitstreams/nonlp-1p-10bit_r2r_test/run3/svt_M10_ducks_take_off_1080p50_60f_10bit_Q62.bin',

]

def main():
    t0 = time.time()
    bitstream_dir = 'bitstreams'
    create_folders(bitstream_dir)   
    formatted_r2r_commands = format_r2r_commands()

    for r2r_command in formatted_r2r_commands:
        preset = re.search(r'--preset\s(-?\d+)', r2r_command).group(1)
        
        if int(preset) > 4:# in ['5','6','7','8','9']:
            upper_limit = 100000
        else:
            upper_limit = 100000
            
        result, encoding_hashes = run_r2r_commands(r2r_command, upper_limit, bitstream_dir,preset)
        print('result',result)
        # print('encoding_hashes',encoding_hashes)
        print(r2r_command)
        print('\n\n\n')

        print('took {}'.format(time.time()-t0))
        
        if encoding_hashes:
            save_r2r_encodings(encoding_hashes, preset)


def create_folders(bitstream_dir):
    # if os.path.isdir(bitstream_dir):
        # shutil.rmtree(bitstream_dir)
    # os.mkdir(bitstream_dir)
    if os.path.isdir("bitstreams"):
        shutil.rmtree('bitstreams')
    os.mkdir("bitstreams")

        
def format_r2r_commands():
    formatted_r2r_commands = []
    
    if presets:
        for r2r_command in r2r_commands:
            for preset in presets:
                r2r_command = re.sub('--preset\s*\d*','--preset {}'.format(preset),r2r_command)
                formatted_r2r_commands.append(r2r_command)
    else:
        formatted_r2r_commands = r2r_commands
        
    return formatted_r2r_commands


def run_r2r_commands(command, upper_limit, bitstream_dir,preset):
    hash_file = {}

    output_stream = ' -b {}'
    r2r_commands = [re.sub(r'-b\s.*?.bin',output_stream.format(os.path.join(bitstream_dir,'{}_M{}.bin'.format(i,preset))),command) for i in range(CPU_COUNT)]
    with open('r2r.txt','w') as r2r_file:
        for r2r_command in r2r_commands:
            r2r_file.write(r2r_command)
            r2r_file.write('\n')
            
    md5_commands = ['md5sum  {}.bin'.format(os.path.join(bitstream_dir,'{}_M{}'.format(i,preset))) for i in range(CPU_COUNT)]
    r2r_commands= [x.replace('output',str(i)) for i,x in enumerate(r2r_commands)]
    print('r2r_commands',r2r_commands)
    
    for count in range(0,upper_limit,CPU_COUNT):
        #encode_output = execute_parallel_commands(CPU_COUNT, r2r_commands, os.getcwd())
        print('On run {}'.format(count+CPU_COUNT))
        run_parallel(CPU_COUNT, 'r2r.txt', 'r2r', encoder_exec_name='svt')
        md5_output = execute_parallel_commands(CPU_COUNT, md5_commands, os.getcwd())
        # print('md5_output',md5_output)
        for md5 in md5_output:
            md5_parts = str(md5).split(' ')
            # print('\n\nmd5_parts',md5_parts)
            hash_file[md5_parts[0]] = md5_parts[-1].strip('\n')

        # print(hash_file)        

        if len(hash_file) > 1:
            result = 'It took {} number of runs to reproduce R2R'.format(count+CPU_COUNT)
            return result, hash_file
        
    result = 'No R2R found after %s number of runs'%(count+CPU_COUNT)
    return result, hash_file
    

def save_r2r_encodings(hash_file, preset):
    source = ''
    destination = os.path.join(os.getcwd(), 'bitstreams/')
    # print(destination)

    for hash in hash_file:
        source_file = hash_file[hash]
    
        try:
            shutil.copy(source_file, destination)
            print("File copied successfully.")
            
        except Exception as e:
            pass
           # print("Error occurred while copying file. source: {} destination: {}".format(source_file,destination),e)


def execute_parallel_commands(number_of_processes, command_list, execution_directory):
    command_lines = [command.strip() for command in command_list]
    execution_directory_list = [execution_directory for i in enumerate(command_lines)]
    inputs = zip(command_lines, execution_directory_list)

    Pooler = multiprocessing.Pool(processes=number_of_processes, maxtasksperchild=30)
    output = Pooler.map(execute_command, inputs)     
    Pooler.close()
    Pooler.join()
    
    if sys.platform == 'Linux':
        os.system('stty sane')

    return output    

def run_parallel(num_pool, run_cmds_file, test_name, encoder_exec_name='svt'):
    if encoder_exec_name == 'aomenc':
        cmd = '/bin/bash -c \'(/usr/bin/time --verbose parallel -j {} < {} ) > time_enc_{}.log 2>&1 \' &'.format(num_pool, run_cmds_file, test_name)
    else:
        cmd = "/bin/bash -c \'(/usr/bin/time --verbose parallel -j {} < {} ) &> time_enc_{}.log\' &".format(num_pool, run_cmds_file, test_name)
    
    execute_command((cmd, os.getcwd()))



def execute_command(inputs):
    cmd, work_dir = inputs

    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()
    pipe.wait() 

    return output



if __name__ == '__main__':
    main()
    
