import datetime
import boto3


'''ARN to send emails to'''
ARN = ''

MACHINE_COSTS = {
    "c5.24xlarge": 4.08,
    "c5.xlarge": 0.17,
    "c5.12xlarge": 2.04,
    "c6i.2xlarge": 0.34,
    "c6i.4xlarge": 0.68,
    "c6i.8xlarge": 1.36,
    "c6i.16xlarge": 2.72,
    "m6i.4xlarge": 0.768,
    "m6i.16xlarge": 3.072,
    "m6id.4xlarge": 0.9492,
    "m6id.metal": 7.5936,
    "t2.micro": 0.0116,
    "m6i.metal": 6.144,
    "c6i.xlarge": 0.17,
    "default": 4.00,
}

def lambda_handler(event, context):
    dates = get_dates()
    '''Get the dates now, one week ago, and the start of the month'''
    instance_size_and_type = get_instance_info()
    '''Get infomation about active instances (storage size, machine type)'''
    variable_dict = get_one_weeks_cost(dates['one_week_ago'], dates['today'], dates['first_of_month'], instance_size_and_type)
    '''use cost explorer to sum and sort machine usage and costs'''

    print("User \t\t\t\t\t\tAmortized Cost \t\t\tUnblended Cost \t\t\tHours Active \t\t\tHours Stopped\n{cost_per_name}\ncost_per_machine \n{cost_per_machine}\ntotal_amortized_cost  {total_amortized_cost}\ntotal_unblended_cost  {total_unblended_cost}\nTotalHours {total_machine_hours}\nother_names \n{other_names}\n".format(**variable_dict))
    variable_dict.update(dates)
    
    send_email(variable_dict)
    '''send email containing all results'''


def get_dates():
    today = datetime.date.today()
    first_of_month = today.replace(day=1)
    
    one_week = datetime.timedelta(days=7)
    one_week_ago = today - one_week
    dates = {
        'one_week_ago':str(one_week_ago),
        'first_of_month':str(first_of_month),
        'today':str(today)
    }
    return dates

def get_instance_info():
    instance_size_and_type = dict()
    instances = ec2.describe_instances()

    for instance in instances['Reservations']:
        tag_name = "Undefined"

        for tag in instance['Instances'][0]['Tags']:
            if tag["Key"] == 'Name':
                tag_name = tag["Value"]

        machine_type = instance['Instances'][0]['InstanceType']
        try:
            machine_cost = MACHINE_COSTS[machine_type]
        except KeyError:
            print('Machine type {} does not have a cost associated with it, using a default value')
            machine_cost = machine_type = "default"
            machine_cost = MACHINE_COSTS[machine_type]
        try:
            if 'Volumeid' in instance['Instances'][0]['BlockDeviceMappings'][0]['Ebs']:
                volume_id = instance['Instances'][0]['BlockDeviceMappings'][0]['Ebs']['VolumeId']
                ebs_details = ec2.describe_volumes(
                    VolumeIds=[
                        volume_id
                    ]
                )
                size = ebs_details['Volumes'][0]['Size']
                instance_size_and_type[tag_name] = [size, machine_cost]
            else:
                print('Failed to find volume ID')
        except IndexError:
            print('Failed to find volume ID')
    return instance_size_and_type


def get_one_weeks_cost(one_week_ago, today, first_of_month, instance_size_and_type):

    cost_per_name = {'paul': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'psao': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'nader': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'daniel': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'hsan': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'colton': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'harvir': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'phoenix': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'amir': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'chanpreet': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'nichole': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'chkn': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'imtl': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'frank': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'matthew': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'sam': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'svt-prod': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'spie2021_aom': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'spie2021_svt': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     'other': {'total_cost': 0, 'hours_active': 0, 'run_cost': 0, 'hours_stopped': 0, 'monthly_cost': 0},
                     }

    cost_per_machine = dict()
    instance_size_and_type, cost_per_machine, total_machine_hours = get_run_time(one_week_ago,today,instance_size_and_type,cost_per_machine)
    
    cost_metric = 'UnblendedCost'
    cost_type = "run_cost"
    cost_per_name,cost_per_machine,other_names,total_unblended_cost = get_costs(one_week_ago,today,cost_per_name,cost_per_machine,cost_metric,cost_type)

    cost_per_name, cost_per_machine = calculate_run_stop_time(cost_per_machine, instance_size_and_type, cost_per_name)

    cost_metric = 'AmortizedCost'
    cost_type = "total_cost"
    cost_per_name,cost_per_machine,other_names,total_amortized_cost = get_costs(one_week_ago,today,cost_per_name,cost_per_machine,cost_metric,cost_type)
    cost_metric = 'AmortizedCost'
    cost_type = "monthly_cost"
    monthly_cost = get_costs(first_of_month,today,cost_per_name,cost_per_machine,cost_metric,cost_type)
    print('Monthly cost is {}'.format(monthly_cost[3]))

    cost_per_name_table, cost_per_machine_table = format_data(cost_per_name, cost_per_machine)

    return_variables = {
        'cost_per_name':cost_per_name_table,
        'cost_per_machine':cost_per_machine_table,
        'total_amortized_cost':round(total_amortized_cost, 2),
        'total_unblended_cost':round(total_unblended_cost, 2),
        'total_machine_hours':round(total_machine_hours, 2),
        'total_monthly_cost':round(monthly_cost[3],2),
        'other_names': list(set(other_names))
    }
    return return_variables

def get_run_time(start_day,today,instance_size_and_type,cost_per_machine):
    total_machine_hours = 0
    hours_used = cost_explorer.get_cost_and_usage(
        TimePeriod={
            'Start': start_day,
            'End': today
        },
        Metrics=['UsageQuantity'],
        Granularity='DAILY',
        Filter={
            'Dimensions': {
                'Key': 'USAGE_TYPE_GROUP',
                'Values': [
                    'EC2: Running Hours',
                ],
            },
        },
        GroupBy=[
            {
                'Type': 'TAG',
                'Key': 'Name'
            },
            {
                'Type': 'DIMENSION',
                'Key': 'INSTANCE_TYPE'
            },
        ],
    )
    for hours in hours_used['ResultsByTime']:
        for group in hours['Groups']:
            found = False
            machine_name = group['Keys'][0].split('Name$')[-1]
            if machine_name == "":  
                continue
            machine_type = group['Keys'][1]
            try:
                machine_cost = MACHINE_COSTS[machine_type]
            except KeyError:
                print('Machine type {} does not have a cost associated with it, using a default value')
                machine_cost = machine_type = "default"
                machine_cost = MACHINE_COSTS[machine_type]

            if machine_name not in instance_size_and_type:
                instance_size_and_type[machine_name] = [300, machine_cost]
            time = group['Metrics']['UsageQuantity']['Amount']

            if 'snapshot' not in machine_name:
                total_machine_hours += float(time)
                if machine_name not in cost_per_machine:
                    cost_per_machine[machine_name] = dict()
                    cost_per_machine[machine_name]["hours_active"] = round(
                        float(time), 1)
                    cost_per_machine[machine_name]["machine_type"] = machine_type
                else:
                    cost_per_machine[machine_name]["hours_active"] += round(
                        float(time), 1)
    return instance_size_and_type, cost_per_machine, total_machine_hours

def get_costs(start_day,today,cost_per_name,cost_per_machine,cost_metric,cost_type):
    other_names = list()
    total_cost = 0
    unblended_cost = cost_explorer.get_cost_and_usage(
        TimePeriod={
            'Start': start_day,
            'End': today
        },
        Metrics=[cost_metric],
        Granularity='DAILY',
        GroupBy=[
            {
                'Type': 'TAG',
                'Key': 'Name'
            },
        ],
    )
    for cost in unblended_cost['ResultsByTime']:
        for group in cost['Groups']:
            found = False
            machine_name = group['Keys'][0].split('Name$')[-1]
            if machine_name == "":
                continue
            cost = group['Metrics'][cost_metric]['Amount']

            if 'snapshot' not in machine_name:
                total_cost += float(cost)
                if cost_per_machine.get(machine_name):  
                    if cost_type not in cost_per_machine[machine_name]:
                        cost_per_machine[machine_name][cost_type] = round(
                            float(cost), 1)
                    else:
                        cost_per_machine[machine_name][cost_type] += round(
                            float(cost), 1)
                else:
                    print("Error getting stats for {}".format(machine_name))

                '''Add cost to member'''
                for user_name, user_stats in cost_per_name.items():
                    if user_name in machine_name.lower():
                        user_stats[cost_type] += round(float(cost), 1)
                        found = True
                if not found:
                    cost_per_name['other'][cost_type] += round(float(cost), 1)
                    other_names.append(machine_name)
    return cost_per_name,cost_per_machine,other_names,total_cost

def calculate_run_stop_time(cost_per_machine, instance_size_and_type, cost_per_name):
    for machine, machine_stats in cost_per_machine.items():
        if 'run_cost' in machine_stats:
            if machine in instance_size_and_type:
                storage_size = instance_size_and_type[machine][0]
                hourly_cost = instance_size_and_type[machine][1]
                storage_cost = storage_size * 0.1 / 30 / 24
            else:
                storage_size = 300
                storage_cost = storage_size * 0.1 / 30 / 24
                hourly_cost = 4.00

            total_hours = machine_stats['hours_active']
            full_cost = machine_stats['run_cost']
            time_active = round(
                float((full_cost - (total_hours*storage_cost))/hourly_cost), 2)
            time_stopped = round(float(total_hours - time_active), 2)

            total_hours = round(float(total_hours), 2)
            
            if time_active < 0:
                time_active = 0
                time_stopped = total_hours
            elif time_stopped < 0:
                time_stopped = 0
                time_active = total_hours

            machine_stats['hours_active'] = time_active
            machine_stats['hours_stopped'] = time_stopped

            '''Add total hours to member'''
            for user_name, user_stats in cost_per_name.items():
                if user_name in machine.lower():
                    user_stats['hours_active'] += time_active
                    user_stats['hours_stopped'] += time_stopped
                    found = True
            if not found:
                cost_per_name['other']['hours_active'] += time_active
                cost_per_name['other']['hours_stopped'] += time_stopped
    return cost_per_name, cost_per_machine

def send_email(variable_dict):
    subject = 'AWS Costs for [{one_week_ago}] - [{today}]'.format(**variable_dict)
    if variable_dict['other_names']:
        variable_dict['uncategorized'] = "\nNames accounting for the 'Other' cost are {other_names}.".format(**variable_dict)
    else:
        variable_dict['uncategorized'] = ""
    if variable_dict['total_monthly_cost'] > 6000:
        variable_dict['over_budget'] = "\nWARNING: AWS Costs this month are higher than the budget permits."
    elif variable_dict['total_amortized_cost'] > 1500:
              variable_dict['over_budget'] = '''\nWARNING: AWS Costs for this week are over the weekly budget of 1500.'''
    else:
        variable_dict['over_budget'] = ''
    message = '''Hi, here are the costs for the following time period [{one_week_ago}] - [{today}]\
    \n\nUSER\t\t\t\t\tAMORTIZED COST($USD)\tHOURS ACTIVE\t\t\tHOURS STOPPED\n{cost_per_name}
    \n\nCOST PER MACHINE\t\t\tAMORTIZED COST($USD)\tHOURS ACTIVE\t\t\tHOURS STOPPED\t\tMACHINE TYPE\
    \n{cost_per_machine}\n
    {over_budget}\nThe overall one week amortized cost is ${total_amortized_cost}.
    \nThe total uptime of all machines was {total_machine_hours} hours.\n
    {uncategorized}\nThe total monthly amortized cost [beginning {first_of_month}] is currently {total_monthly_cost}.
    .'''.format(**variable_dict)

    response = sns.publish(
        TopicArn=ARN,
        Subject=subject,
        Message=message
    )
    return response

def format_data(cost_per_name, cost_per_machine):
    sorted_cost_per_name = dict(sorted(cost_per_name.items(), key=lambda item: item[1]['total_cost'], reverse=True))
    sorted_cost_per_machine = dict(sorted(cost_per_machine.items(), key=lambda item: item[1]['total_cost'], reverse=True))

    cost_per_name_formatted = list(sorted_cost_per_name.items())
    cost_per_machine_formatted = list(sorted_cost_per_machine.items())

    cost_per_name_formatted = [[x[0], round(x[1]['total_cost']), round(
        x[1]['hours_active']), round(x[1]['hours_stopped'])] for x in cost_per_name_formatted]
    cost_per_machine_formatted = [[x[0], round(x[1]['total_cost']), round(
        x[1]['hours_active']), round(x[1]['hours_stopped']), x[1]['machine_type']] for x in cost_per_machine_formatted]

    cost_per_name_table = text_frame(cost_per_name_formatted)
    cost_per_machine_table = text_frame(cost_per_machine_formatted)

    return cost_per_name_table, cost_per_machine_table

def text_frame(string_lst):
    text_table = ''
    for row in string_lst:
        if len(row[0]) > 0 and (row[0][-1] == 'x'):
            number_of_tabs = 4
        elif len(row[0]) <= 7:
            number_of_tabs = 5
        elif len(row[0]) <= 14:
            number_of_tabs = 4
        elif len(row[0]) <= 16:
            number_of_tabs = 3
        elif len(row[0]) <= 20:
            number_of_tabs = 3
        else:
            number_of_tabs = 2

        if len(row) == 5:
            row = row[0] + '\t'*number_of_tabs + str(row[1]) + '\t'*4 + str(
                row[2]) + '\t'*4 + str(row[3]) + '\t'*4 + str(row[4])
        else:
            row = row[0] + '\t'*number_of_tabs + str(row[1]) + '\t'*4 + str(
                row[2]) + '\t'*4 + str(row[3])
        text_table += row  # row_format.format(*row)
        text_table += '\n'
    return text_table


cost_explorer = boto3.client('ce')

ec2 = boto3.client('ec2')

sns = boto3.client('sns')

if __name__ == '__main__':
    lambda_handler("", "")
