import os
import glob
import re
import subprocess
from collections import OrderedDict

target_presets = [1]
target_bitrate = 2000
resolutions = ['1920','1280']

tools_folder = '/mnt/0ea5c24b-9d41-457d-8d9f-f665682da123/CVH/tools'
# target_qp = 47
    
    
def parse_txt_file(txt_file_path):
    """
    Parses the .txt file to extract width, height, and bitrate.
    """
    with open(txt_file_path, 'r') as file:
        content = file.read()

    try:
        # Extract width and height
        # wh_match = re.search(r'width / height\s*:\s*(\d+)\s*/\s*(\d+)', content)
        width = content.split('width / height')[1].split('\n')[0].split(': ')[1].split('/')[0].strip()
        height = content.split('width / height')[1].split('\n')[0].split(': ')[1].split('/')[1].strip()

        # Extract bitrate
        # bitrate_match = re.search(r'Total Frames.*?([\d\.]+)\s*kbps', content, re.DOTALL)
        bitrate = content.split('Total Frames')[1].split('kbps')[0].split('\t')[-1].strip()

        return width, height, bitrate
    except AttributeError:
        print(f"Failed to parse {txt_file_path}. Skipping file.")
        return None, None, None


def extract_info_from_filename(filename):
    """
    Extracts clip name and qp value from the filename.
    Expected filename format: M<preset>_<clip>_Q<qp>.txt
    """
    # qp_found = re.search(r'M-?\d+_*(.*?)_*Q(\d+)\..*?', os.path.split(filename)[-1])
    qp_found = re.search(r'M-?\d+_*(.*?)_*RC(\d+)\..*?', os.path.split(filename)[-1])
    
    if qp_found:
        clip = qp_found.group(1).split('_1920')[0]
        qp = qp_found.group(2)
        return clip, qp
    else:
        print(f"Filename {filename} does not match expected pattern. Skipping file.")
        return None, None


def get_fps(file):
    ffprobe_output = subprocess.Popen( '{}/ffmpeg -i {}'.format(tools_folder, str(file), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE ))
    clip_info = str(ffprobe_output.communicate()[1])
    fps = re.search(r'(\d+\.?\d*) tbr',clip_info).group(1)
    return fps
    
    
def main():

    current_dir = os.getcwd()
    folders = glob.glob(f'{current_dir}/*')

    bitstream_selections = OrderedDict()
    for resolution in resolutions:
        for preset in target_presets:
            preset_folders = sorted([folder for folder in folders if f'm{preset}' in os.path.basename(folder).lower()])
            bitstream_selections[preset] = {}

            for folder in preset_folders:
                print(f'Processing folder: {folder}')
                bitstreams_dir = os.path.join(folder, 'bitstreams')
                txt_files = glob.glob(f'{bitstreams_dir}/*.txt')

                for txt_file in txt_files:
                    width, height, bitrate = parse_txt_file(txt_file)
                    if not all([width, height, bitrate]):
                        continue  # Skip if parsing failed

                    # Only consider width of 1920/1280
                    if width != resolution:
                        continue
                    
                    if f'_M{preset}_' not in os.path.split(txt_file)[-1]:
                        continue

                    filename = os.path.basename(txt_file)
                    clip, qp = extract_info_from_filename(filename)

                    if not all([clip, qp]):
                        continue  # Skip if extraction failed
   
                    if clip not in bitstream_selections[preset]:
                        bitstream_selections[preset][clip] = {
                            'bitstream': txt_file.replace('.txt', '.bin'),
                            'bitrate': bitrate,
                            'qp': qp
                        }
                    else:
                        current_best = bitstream_selections[preset][clip]
                        if abs(float(bitrate) - target_bitrate) < abs(float(current_best['bitrate']) - target_bitrate):
                            bitstream_selections[preset][clip] = {
                                'bitstream': txt_file.replace('.txt', '.bin'),
                                'bitrate': bitrate,
                                'qp': qp
                            }

        # Print results
        for preset, clips in bitstream_selections.items():
            concat_files = []
            print(f"\nPreset: {preset}")
            for clip, data in clips.items():
                print(f"Clip: {clip}")
                print(f"  Bitstream: {data['bitstream']}")
                print(f"  Bitrate: {data['bitrate']} kbps")
                print(f"  QP: {data['qp']}")
                # print(f"  fps: {data['fps']}")
                
                concat_files.append(f"file '{data['bitstream']}'")

            # Write the file list for this preset
            concat_filename = f'concat_list_preset_{preset}_{resolution}.txt'
            with open(concat_filename, 'w') as concat_file:
                concat_file.write("\n".join(sorted(concat_files)))

            # Generate the ffmpeg concat command for this preset
            ffmpeg_command = f"{tools_folder}/ffmpeg -f concat -safe 0 -i {concat_filename} -c copy -f ivf output_preset_{preset}_{resolution}.bin"
            # input()
            os.system(ffmpeg_command)
            print(f"\nFFmpeg Command for Preset {preset}:")
            print(ffmpeg_command)
            
if __name__ == "__main__":
    main()
