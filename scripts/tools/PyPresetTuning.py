import binascii
import os
import subprocess
import re
import glob
import csv
import smtplib
import math
import sys
import shutil
import argparse
import datetime
import time

import PyCompareResults

EMAIL_SENDER = 'AutoSender@intel.com'
EMAIL_RECIPIENTS = []


def TEST_REPO():

    KEYINT = {'no_intra_refresh' : -1,
          '1 second' : '1s',
          '2 second' : '2s',
          '3 second' : '3s',
          'tuning' : 200,
          }

    CLIPSET = {
    # '1080p' : 'ULTRA-full_elfuente',
    # 'M5_tuning' : 'ULTRA_480P_elfuente',
    # '720p' : 'ULTRA_720P_elfuente',
    
    'full' : 'ULTRA_2023-12-05_Full_y4m',
    'large' : 'ULTRA_2023-12-05_Large_y4m',
    'medium' : 'ULTRA_2023-12-05_Medium_y4m',
    'small' : 'ULTRA_2023-12-05_Small_y4m',
    'tiny' : 'ULTRA_2023-12-05_Tiny_y4m',
    
    
        }

    TESTS = {

    'svt_CRF_1lp_1p_tuning_11qp_FULL'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['full'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_11qp_LARGE'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['large'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_11qp_MEDIUM'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['medium'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_11qp_SMALL'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['small'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_11qp_TINY'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['tiny'], KEYINT['tuning']],

    'svt_CRF_1lp_1p_tuning_5qp_FULL'    : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['full'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_5qp_LARGE'    : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['large'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_5qp_MEDIUM'    : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['medium'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_5qp_SMALL'    : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['small'], KEYINT['tuning']],
    'svt_CRF_1lp_1p_tuning_5qp_TINY'    : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['tiny'], KEYINT['tuning']],



    # 'svt_CRF_lp8_1p_tuning_5qp_M10_M13' : ['svt_CRF_lp8_1p_tuning_5qp', CLIPSET['full'], KEYINT['tuning']],
    # 'svt_CRF_lp8_1p_tuning_5qp_M7_M10' : ['svt_CRF_lp8_1p_tuning_5qp', CLIPSET['large'], KEYINT['tuning']],
    # 'svt_CRF_lp8_1p_tuning_5qp_M4_M7' : ['svt_CRF_lp8_1p_tuning_5qp', CLIPSET['medium'], KEYINT['tuning']],
    # 'svt_CRF_lp8_1p_tuning_5qp_M1_M4' : ['svt_CRF_lp8_1p_tuning_5qp', CLIPSET['small'], KEYINT['tuning']],
    # 'svt_CRF_lp8_1p_tuning_5qp_MR_M1' : ['svt_CRF_lp8_1p_tuning_5qp', CLIPSET['tiny'], KEYINT['tuning']],
    # 'svt_CRF_1lp_1p_tuning_11qp'    : ['svt_CRF_1lp_1p_tuning_11qp', CLIPSET['720p'], KEYINT['tuning']],
    # 'svt_CRF_1lp_1p_tuning_5qp_720p' : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['720p'], KEYINT['tuning']],
    # 'svt_CRF_1lp_1p_tuning_5qp_1080p' : ['svt_CRF_1lp_1p_tuning_5qp', CLIPSET['1080p'], KEYINT['tuning']],

    }
    
    return TESTS

#svt_CRF_1lp_1p_tuning_5qp
def main():
    insert_special_params = ''
    
    configurations = ['']
    git_url = ''
    mr_number = ""
    branch = ''
    commit = ''
    master = ''
    tune_presets = []  # [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
    

    for tune_preset in tune_presets:

        for test_name in configurations:
            '''master '''
            if master:
                clone_repo(git_url, branch, master, mr_number)

                folder_name = 'M{}_master'.format(tune_preset)#os.path.split(patch)[-1].split('.')[0]
                parent_folder, test_folder,exists = create_folder(folder_name, test_name, tune_preset)
                
                if not exists:
                    print("================ Running {} on patch {}================".format(configurations, folder_name))

                    build_status = build_encoder(test_folder)
                    copy_files_to_test_folder(test_folder)

                    generate_and_execute_commands(test_name, test_folder, tune_preset, insert_special_params)
                    result_files = get_result_files(parent_folder)
                    
                    copy_result_files(result_files, tune_preset)
                    
                else:
                    print('{} already tested, skipping...'.format(test_folder))

            '''Main Loop'''
            t0 = time.time()
            encoding_md5s = []
            
            clone_repo(git_url, branch, commit, mr_number)
            preset_conditional_lines = get_preset_conditional_lines(tune_preset)
            patch_files = generate_patches(preset_conditional_lines, tune_preset)

            for patch in patch_files:

                folder_name = os.path.split(patch)[-1].split('.')[0]
                parent_folder, test_folder,exists = create_folder(folder_name, test_name, tune_preset)

                if exists:
                    if 'as_is' in patch:
                        encoding_md5s = check_for_lossless(test_folder, tune_preset, encoding_md5s, test_name, insert_special_params)
                    print('{} already tested, skipping...'.format(test_folder))
                    continue  
                    
                print("================ Running {} on patch {}================".format(configurations, patch))

                apply_patch(patch)
                build_status = build_encoder(test_folder)
                copy_files_to_test_folder(test_folder)
                encoding_md5s = check_for_lossless(test_folder, tune_preset, encoding_md5s, test_name, insert_special_params)

                if is_lossless(encoding_md5s):
                    print('{} is lossless, skipping'.format(patch))
                    print('Deleting lossless dir {}'.format(test_folder))
                    shutil.rmtree(test_folder)
                    continue

                generate_and_execute_commands(test_name, test_folder, tune_preset, insert_special_params)
                result_files = get_result_files(parent_folder)
                
                copy_result_files(result_files, tune_preset)

            print('took {}'.format(time.time() - t0))



def get_result_files(folder, mod_commit = None ,ref_commit=None ):
    result_files = []
    for root, _, files in os.walk(folder):
        for file in files:
            if mod_commit:
                if '_result' in file and ((mod_commit[:5] in file or ref_commit[:5] in file) or (mod_commit[:5] in os.path.split(root)[-1] or ref_commit[:5] in os.path.split(root)[-1]) ):
                    result_files.append(os.path.join(root, file))
            else:
                if '_result' in file:
                    result_files.append(os.path.join(root, file))                
    return result_files
            
def check_for_lossless(test_folder, tune_preset, encoding_md5s, configuration, insert_special_params):
    t1 = time.time()
    clip_720p = None
    clip_480p = None
    preset_tuning_lossless_check_dir = '/dev/shm/preset_tuning_lossless_check'
    dir = preset_tuning_lossless_check_dir
    if not os.path.isdir(preset_tuning_lossless_check_dir):
        os.mkdir(preset_tuning_lossless_check_dir)
        
    test_name, stream_dir, keyint = TEST_REPO()[configuration]
    stream_dir = find_stream_folder(stream_dir)
    clips = glob.glob('{}/*'.format(stream_dir))
    
    class_240p_th = 0x28500
    class_360p_th = 0x4ce00
    class_480p_th = 0xa1400
    class_720p_th = 0x16da00
    class_1080p_th = 0x535200
    class_4k_th = 0x140a000
    
    test_clips = dict()
    for clip in clips:
        if clip.endswith('y4m'):
            width, height, fps, frames = read_y4m_header(clip)
            
            pixels_per_frame = int(width) * int(height)
            
            if pixels_per_frame < class_240p_th:
                test_clips['clip_240p'] = clip
            elif pixels_per_frame < class_360p_th:
                test_clips['clip_360p'] = clip
            elif pixels_per_frame < class_480p_th:
                test_clips['clip_480p'] = clip
            elif pixels_per_frame < class_720p_th:
                test_clips['clip_720p'] = clip
            elif pixels_per_frame < class_1080p_th:
                test_clips['clip_1080p'] = clip
            elif pixels_per_frame < class_4k_th:
                test_clips['clip_4k'] = clip
            else:
                continue          

    if 'as_is' in test_folder:
        tune_preset = re.search(r'M(R?-?\d*)_as_is', test_folder).group(1)
        if tune_preset == 'R':
            tune_preset = '-1'
    print('tune_preset',tune_preset)
            
    # if not clip_1080p or not clip_240p:
        # print('clip_720p not found, exiting')
        # sys.exit(-1)

    quality_levels = [23, 27, 31, 35, 39, 43, 47, 51, 55, 59, 63]

    commands = [
        './SvtAv1EncApp --preset {tune_preset} {insert_special_params} -q {q} --keyint 30 --lp 0 --passes 1'.format(
            tune_preset=tune_preset,
            insert_special_params=insert_special_params,
            q=q
        ) for q in quality_levels
    ]
    #command_template = '{cmd} -i {clip_1080p} -b {dir}/1080p_q{q}.bin & {cmd} -i {clip_480p} -b {dir}/480p_q{q}.bin & {cmd} -i {clip_240p} -b {dir}/240p_q{q}.bin & {cmd} -i {clip_480p} -b {dir}/480p_q{q}.bin & {cmd} -i {clip_480p} -b {dir}/480p_q{q}.bin'
    command_template = ''
    
    for test_clip in test_clips:
         command_template += '{cmd} -i %s -b {dir}/%s_q{q}.bin & ' % (test_clips[test_clip], test_clip)
    
    command_template = command_template[:-3]
    
    # Generate full command string
    fast_encode_command = 'cd {test_folder} && ('.format(test_folder=test_folder) + ' & '.join([command_template.format(**vars()) for cmd, q in zip(commands, quality_levels)]) + ')'
    print('\n')
    print(fast_encode_command)
    print('\n')
    md5_check = 'md5sum {dir}/*.bin'.format(**vars())

    print('running {}'.format(fast_encode_command))
    los_t0 = time.time()
    call(fast_encode_command)
    print('lossless tool {}'.format(time.time() - los_t0 ))
    print('running {}'.format(md5_check))
    
    encoding_md5 = ''.join(call(md5_check))#[0]#.split(' ')[0]
    
    encoding_md5s.append(encoding_md5)
    
    print('\n')
    print('encoding_md5',encoding_md5)
    print('check took', time.time()-t1)
    print('\n')
    
    return encoding_md5s

'''Two functions to extract clip info from its y4m header'''
def read_y4m_header_helper(readByte, buffer):
    if sys.version_info[0] == 3:
        if (readByte == b'\n' or readByte == b' '):
            clip_parameter = buffer
            buffer = b""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer
    else:
        if (readByte == '\n' or readByte == ' '):
            clip_parameter = buffer
            buffer = ""
            return clip_parameter, buffer
        else:
            buffer += readByte
            return -1, buffer


def read_y4m_header(clip):
    if sys.version_info[0] == 3:
        header_delimiters = {b"W": 'width', b"H": 'height', b"F": 'frame_ratio', b"I": 'interlacing', b"A": 'pixel_aspect_ratio', b"C": 'bitdepth'}
    else:
        header_delimiters = {"W": 'width', "H": 'height', "F": 'frame_ratio', "I": 'interlacing', "A": 'pixel_aspect_ratio', "C": 'bitdepth'}

    y4m_params = {'width': -1,
                  'height': -1,
                  'frame_ratio': -1,
                  'framerate': -1,
                  'number_of_frames': 1,
                  'bitdepth': -1
                  }

    with open(clip, "rb") as f:
        f.seek(10)

        if sys.version_info[0] == 3:
            buffer = b""
        else:
            buffer = ""

        while True:
            readByte = f.read(1)
            if (readByte in header_delimiters.keys()):
                y4m_key = readByte
                while True:
                    readByte = f.read(1)

                    '''Use helper function to interpret byte'''
                    y4m_params[header_delimiters[y4m_key]], buffer = read_y4m_header_helper(readByte, buffer)

                    if y4m_params[header_delimiters[y4m_key]] != -1:
                        break

            if sys.version_info[0] == 3:
                if binascii.hexlify(readByte) == b'0a':
                    break
            else:
                if binascii.hexlify(readByte) == '0a':
                    break

        if sys.version_info[0] == 3:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(b":")
            if b'10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'
        else:
            frame_ratio_pieces = y4m_params['frame_ratio'].split(":")
            if '10' in y4m_params['bitdepth']:
                frame_length = int(float(2) * float(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2))
                y4m_params['bitdepth'] = '10bit'
            else:
                frame_length = int(int(y4m_params['width']) * int(y4m_params['height']) * float(3) / 2)
                y4m_params['bitdepth'] = '8bit'

        y4m_params['framerate'] = float(frame_ratio_pieces[0]) / float(frame_ratio_pieces[1])

        while f.tell() < os.path.getsize(clip):
            readByte = f.read(1)
            if binascii.hexlify(readByte) == b'0a':
                f.seek(frame_length, 1)
                buff = binascii.hexlify(f.read(5))
                if buff == b'4652414d45':
                    y4m_params['number_of_frames'] += 1

    return int(y4m_params['width']), int(y4m_params['height']), y4m_params['framerate'], y4m_params['number_of_frames']

def is_lossless(encoding_md5s):
    if len(encoding_md5s) == 1:
        return False
    if encoding_md5s[-1] in encoding_md5s[0:-1]:
        return True
    else:
        return False


def copy_result_files(result_files, tune_preset):
    result_folder = 'preset_tuning_M{}_results'.format(tune_preset)
    source_patches_folder = os.path.join(os.getcwd(),'svt','patches')
    source_patches = glob.glob('{}/*.patch'.format(source_patches_folder))
    preset_patches_folder = os.path.join(result_folder, 'patches')
    if not os.path.isdir(result_folder):
        os.mkdir(result_folder)
    if not os.path.isdir(preset_patches_folder):        
        os.mkdir(preset_patches_folder)
    
    for file in result_files:
        shutil.copy(file, result_folder)
    for file in source_patches:
        shutil.copy(file, preset_patches_folder)


def clone_repo(git_url, branch, commit, mr):
    
    subprocess.call("rm -rf svt", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
    subprocess.call("git clone {} svt".format(git_url), shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
    if mr:
        subprocess.call("git -C svt fetch origin merge-requests/{}/head".format(mr), shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
        subprocess.call("git -C svt checkout FETCH_HEAD", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
        
    if branch:
        subprocess.call("git -C svt checkout {}".format(branch), shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
    if commit:
        subprocess.call("git -C svt checkout {}".format(commit), shell=True)
    subprocess.call("mkdir -p svt/patches", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))


def get_preset_conditional_lines(tune_preset):
    if tune_preset == -1:
        preset_as_is = 'ENC_MR'
        preset_below = '-2'
        
    elif tune_preset == 0:
        preset_as_is = 'ENC_M0'
        preset_below = 'ENC_MR'
    elif tune_preset == 7:
        preset_as_is = 'ENC_M7'
        preset_below = 'ENC_M5'
    elif tune_preset == 13:
        preset_as_is = 'ENC_M13'
        preset_below = 'ENC_M11'
    else:
        preset_as_is = 'ENC_M{}'.format(tune_preset)    
        preset_below = 'ENC_M{}'.format(tune_preset - 1)
        
    presets = [preset_as_is, preset_below]
    subprocess.call("git -C svt reset --hard", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))

    find_feature_name = 0    
    patch_file_lines = []
    
    target_dirs = [os.path.join(os.getcwd(), "svt/Source/Lib/Encoder/Codec"),
                   os.path.join(os.getcwd(), "svt/Source/Lib/Encoder/Globals")]    
    
    for target_dir in target_dirs:
        for file in glob.glob('{}/*'.format(target_dir)):
            with open(file) as lines:
                line_queue = []
                for line_number, line in enumerate(lines):
                    for preset in presets:
                        if re.search(r'<=\s*ENC_M[R]?\d*',line) and preset in line and '<' in line and r'#' not in line and '/' not in line:
                            preset_conditional_line = line_number
                            line_queue.append(preset_conditional_line)
                            find_feature_name = 1

                        if find_feature_name and '=' in line and '<' not in line and 'if ' not in line:
                            feature_name = line.split('=')[0].strip().split('>')[-1].replace('/','')
                            find_feature_name = 0
                            for queue in line_queue:
                                patch_file_lines.append([file, queue, feature_name])
                                line_queue = []

    return patch_file_lines


def generate_patches(patch_file_lines, tune_preset):
    patch_files = []
    features_to_skip = ['mem_max_can_count','pallet',]
    subprocess.call("git -C svt reset --hard", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
    print('tune_presetss',tune_preset)
    tune_preset = int(tune_preset)
    if tune_preset == -1:
        preset_as_is = 'ENC_MR'
        preset_below = '-2'
        preset_above = 'ENC_M0'
        
    elif tune_preset == 0:
        preset_as_is = 'ENC_M0'
        preset_below = 'ENC_MR'
        preset_above = 'ENC_M1'
    elif tune_preset == 7:
        preset_as_is = 'ENC_M7'
        preset_below = 'ENC_M5'
        preset_above = 'ENC_M8'
    elif tune_preset == 13:
        preset_as_is = 'ENC_M13'
        preset_below = 'ENC_M11'
        preset_above = 'ENC_M14'        
    else:
        preset_as_is = 'ENC_M{}'.format(tune_preset)    
        preset_below = 'ENC_M{}'.format(tune_preset - 1)
        preset_above = 'ENC_M{}'.format(tune_preset + 1)
               
    as_is_patch_default = os.path.join(os.getcwd(),'svt','patches','{}_as_is.patch'.format(preset_as_is))
    as_is_patch_default_1 = os.path.join(os.getcwd(),'svt','patches','{}_as_is.patch'.format(preset_below))
    
    subprocess.call("git -C svt diff > {}".format(as_is_patch_default), shell=True)
    subprocess.call("git -C svt diff > {}".format(as_is_patch_default_1), shell=True)
    
    patch_files.append(as_is_patch_default)
    patch_files.append(as_is_patch_default_1)
    
    print('preset_as_is',preset_as_is)
    print('preset_below',preset_below)

    for file_name,line_number, feature_name in patch_file_lines:
        subprocess.call("git -C svt reset --hard", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))

        patch_name = None
        with open(file_name, 'r') as file:
            lines = file.readlines()

        if preset_as_is in lines[line_number]:
            lines[line_number] = lines[line_number].replace(preset_as_is, preset_below)
            patch_name = '{}_{}_{}_{}.patch'.format(feature_name, line_number + 1, preset_as_is, preset_above)

        elif preset_below in lines[line_number]:
            print('yup')
            lines[line_number] = lines[line_number].replace(preset_below, preset_as_is)
            patch_name = '{}_{}_{}_{}.patch'.format(feature_name,line_number+ 1, preset_as_is, preset_below)

            
        with open(file_name, 'w') as file:
            file.writelines(lines)
            
        for skip_feature in features_to_skip:
            if skip_feature in feature_name:
                continue
        if not patch_name:
            continue
            
        patch_file = os.path.join(os.getcwd(),'svt','patches',patch_name.replace(' ',''))
        name, ext = os.path.splitext(patch_file)
        name = name.replace('.', '')  # Remove all periods in the filename
        patch_file = '{}.patch'.format(name)

        if os.path.isfile(patch_file):
            print('patch_file',patch_file)
            print('we are about to overwrite')

        subprocess.call("git -C svt diff > {}".format(patch_file), shell=True)#, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
        patch_files.append(patch_file)

    subprocess.call("git -C svt reset --hard", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))
                
    return patch_files


def apply_patch(patch):
    subprocess.call("git -C svt reset --hard", shell=True, stdout=open(os.devnull, "w"), stderr=open(os.devnull, "w"))

    subprocess.call("git -C svt apply {} -C1 --ignore-whitespace".format(patch), shell=True)
    

def build_encoder(test_folder):
    build_file_path = os.path.join(os.getcwd(),'svt','Build','linux','build.sh')
    bin_folder = os.path.join(os.getcwd(),'svt','Bin','Release')
    repo_folder = os.path.join(os.getcwd(),'svt')

    pipe = subprocess.Popen('chmod +x {}'.format(build_file_path), shell=True, cwd=os.getcwd(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    pipe.wait()

    pipe = subprocess.Popen('{} static'.format(build_file_path), shell=True, cwd=os.getcwd(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()

    pipe.wait()
    
    if 'failed' in str(output) or 'failed' in str(error):
        print('BUILD FAILED')
        return False
    
    for file in glob.glob('{}/*'.format(bin_folder)):
        shutil.copy(file, test_folder)
    
    return True


def create_folder(folder_name, test_name, tune_preset):
    parent = 'preset_tuning_M{}'.format(tune_preset)

    parent_folder = os.path.join(os.getcwd(),parent)
    child_folder = os.path.join(parent_folder, folder_name)
    if os.path.isdir(child_folder):
        return parent_folder, child_folder, True
        
    if not os.path.isdir(parent_folder):
        os.mkdir(parent_folder)
    if not os.path.isdir(child_folder):
        os.mkdir(child_folder)

    return parent_folder, child_folder, False



def copy_files_to_test_folder(test_folder):
    # Copy necessary files to test folder
    test_tools_folder = os.path.join(test_folder,'tools')
    
    if not os.path.isdir(test_tools_folder):
        os.mkdir(test_tools_folder)

    for file in glob.glob('{}/*'.format(os.path.join(os.getcwd(), 'tools'))):
        shutil.copy(file, test_tools_folder)
    for file in glob.glob('{}/*.py'.format(os.getcwd())):
        shutil.copy(file, test_folder)        


def generate_and_execute_commands(configuration, test_folder, tune_preset, insert_special_params):
   
    # if tune_preset == 'R':
        # tune_preset = '-1'
    test_name, stream_dir, keyint = TEST_REPO()[configuration]

    print('test_name',test_name)
    
    if 'as_is' in test_folder:
        presets = re.search(r'M(R?-?\d*)_as_is', test_folder).group(1)
        if tune_preset == 'R':
            presets = '-1'
    else:
        presets = tune_preset

    print('stream_dir',stream_dir)
    stream_dir = find_stream_folder(stream_dir)
    print('stream_dir',stream_dir)

    ### Make this a python call function
    mod_pygen = 'python3 PyGenerateCommands.py --test-name {test_name} --stream {stream_dir} --presets {presets} --intraperiod {keyint} --added_params "{insert_special_params}" --run 2'.format(**vars())
    

    print('running {}'.format(mod_pygen))
    call(mod_pygen, test_folder)
    call('rm -rf bitstreams', test_folder)


def find_stream_folder(stream_dir):
    for root, _, files in os.walk('/home/'):
        if stream_dir in root:
            return root
    for root, _, files in os.walk('/media/'):
        if stream_dir in root:
            return root
    for root, _, files in os.walk('/mnt/'):
        if stream_dir in root:
            return root
def call(command, work_dir=os.getcwd()):
    pipe = subprocess.Popen(command, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()
    pipe.wait()
    
    try:
        return str(output.decode("utf-8")).splitlines()
    except:
        return str(output).splitlines()


if __name__ == "__main__":
    main()
