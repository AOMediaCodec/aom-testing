#!/usr/bin/env python

# Copyright (c) 2022, Alliance for Open Media. All rights reserved
#
# This source code is subject to the terms of the BSD 2 Clause License and
# the Alliance for Open Media Patent License 1.0. If the BSD 2 Clause License
# was not distributed with this source code in the LICENSE file, you can
# obtain it at www.aomedia.org/license/software. If the Alliance for Open
# Media Patent License 1.0 was not distributed with this source code in the
# PATENTS file, you can obtain it at www.aomedia.org/license/patent.

"""
Script for macro cleanup and clang-format for the SVT-AV1 project.

Tested against e7e99798c9f0df35c08a303026b39f525e433225.

Run by calling `python svt_av1_macro_cleanup.py <tag>` from the root of the project.
Assumes git, gcc, clang-format, and unifdef are installed and in the PATH.
"""

import subprocess
import sys
import os
import argparse
import re


def main():
    """Main function for the script."""
    parser = argparse.ArgumentParser(
        prog="svt_av1_macro_cleanup.py",
        description="Macro cleanup and clang-format for SVT-AV1",
        epilog="Example: python svt_av1_macro_cleanup.py v1.3.0")
    parser.add_argument("-p", dest="push", action="store_true",
                        help="Push the changes to the remote")
    parser.add_argument("tag", type=str, nargs="?", default=get_latest_tag_plus_one(),
                        help="tag to use for commit message, defaults to last tag plus one patch")
    args = parser.parse_args()

    if args.tag == "unknown" or len(args.tag.strip()) == 0:
        print("No tag found, please specify a tag")
        sys.exit(1)

    tag = args.tag.strip()  # type: str
    print("Checking out new branch")
    call_git("checkout", "-fb", "macro_cleanup/" + tag)

    # Main function
    print("Cleaning up macros")
    macro_cleanup()

    call_git("commit", "-asm", "tag: macro cleanup " +
             tag, exceptions_expected=True)

    print("Running clang-format")
    clang_format()

    call_git("commit", "-asm", "tag: clang-format " +
             tag, exceptions_expected=True)

    if args.push:
        print("Pushing changes")
        call_git('push', '-uf', 'origin', 'macro_cleanup/' + tag)


# helper functions


def call_git(*args, exceptions_expected=False):
    # type: (list[str], bool) -> list[str]
    """calls git with the given arguments, can raise CalledProcessError"""
    if not exceptions_expected:
        return subprocess.check_output(["git"] + list(args)).decode("utf-8").splitlines()
    try:
        return subprocess.check_output(["git"] + list(args)).decode("utf-8").splitlines()
    except subprocess.CalledProcessError:
        print("Nothing to do")
        return []


def call_unifdef(*args, macro_file="t.c"):
    # type: (list[str], str) -> None
    """
    calls unifdef with the given arguments.
    Does not check return because unifdef returns 1 if it was successful
    and a 0 if it did nothing.
    """
    subprocess.call(["unifdef", "-m", "-f" + macro_file] + list(args))


def call_clang_format(*args):
    # type: (list[str]) -> None
    """calls clang-format with the given arguments, can raise CalledProcessError"""
    subprocess.check_call(["clang-format", "-i"] + list(args))


def call_func_for_sourcefiles(function_to_call):
    # type: (function) -> None
    """Calls the given function for all files in the Source+test directory."""
    assert callable(function_to_call), "function_to_call must be a function"
    for path in ("Source", "test"):
        for root, _, files in os.walk(path):
            for file in files:
                if file.endswith((".c", ".h", ".cpp", ".cc", ".hpp")):
                    function_to_call(os.path.join(root, file))

# processing functions


def get_latest_tag_plus_one():
    """
    Returns the latest tag in the current branch with the patch ver incremented.

    Ignores tags that are not in the format v[0-9]+\\.[0-9]+\\.[0-9]+
    """
    tag_list = call_git("tag", "-l", r"v[0-9]*\.[0-9]*\.[0-9]*")
    tag_list.reverse()
    pattern = re.compile(r"v[0-9]+\.[0-9]+\.[0-9]+")
    latest_tag = None
    for tag in tag_list:
        if pattern.match(tag):
            latest_tag = tag
            break
    if latest_tag is None:
        return "unknown"  # perhaps needs git fetch --tags
    latest_tag = list(map(int, latest_tag.replace("v", "").split(".")))
    if len(latest_tag) != 3:
        return "unknown"  # don't know how to handle this
    latest_tag[-1] += 1
    return "v" + ".".join(map(str, latest_tag))


def macro_cleanup():
    """
    Runs unifdef on all files in the Source and test directories.
    Also cleanups up EbDebugMacros.h.
    """
    call_git("reset", "--hard", "HEAD")
    debug_macros = "Source/API/EbDebugMacros.h"

    debug_macro_lines = []
    macro_file_lines = []

    with open(debug_macros, "r+t") as debug_macros_file:
        lines = debug_macros_file.readlines()
        send_to_macro_file = False
        seen_cplusplus = False

        for line in lines:
            if line.startswith("#endif // __cplusplus") and not seen_cplusplus:
                send_to_macro_file = True
                seen_cplusplus = True

                debug_macro_lines.append(line)
                debug_macro_lines.append("\n")

                continue

            if line.startswith("//FOR DEBUGGING - Do not remove"):
                send_to_macro_file = False

            if send_to_macro_file:
                macro_file_lines.append(line)
            else:
                debug_macro_lines.append(line)

        debug_macros_file.truncate(0)
        debug_macros_file.seek(0)
        debug_macros_file.writelines(debug_macro_lines)

    gcc_input = '\n'.join(macro_file_lines).encode("utf-8")

    gcc = subprocess.Popen(["gcc",  "-fpreprocessed", "-fdirectives-only", "-dD", "-E", "-P", "-"],
                           stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    output = gcc.communicate(input=gcc_input)[0].decode("utf-8")

    with open("t.c", "w") as separate_file:
        separate_file.writelines(output)

    call_func_for_sourcefiles(call_unifdef)
    os.remove("t.c")


def clang_format():
    """Runs clang-format on all files in the Source and test directories."""
    call_git("reset", "--hard", "HEAD")
    call_func_for_sourcefiles(call_clang_format)


if __name__ == "__main__":
    main()
