import os
import json
import hashlib
import multiprocessing
import shutil
import subprocess
import tarfile
import time
import tqdm

TARGET_FOLDERS = [r'C:\\']
# list of folders to search for duplicates in
COMPRESSION_FLAG = (False, "Folder")
# if true, all bitstream folders will be compressed and the originals deleted
# Second argument can be either 'File' or 'Folder' and determines the compression method (entire folder or file-by-file)
# for individual 'file' compression, zstd.exe must be exist in the script's directory

ZSTD_EXE = os.path.join(os.getcwd(), 'zstd.exe')

STORAGE_DIRECTORY = r'C:\\'

COMBINED_FOLDER_NAMES = "_".join(
    [folder.rsplit(os.sep, maxsplit=1)[-1] for folder in TARGET_FOLDERS])

BACK_UP_PATH = os.path.join(STORAGE_DIRECTORY, COMBINED_FOLDER_NAMES)

INFO_FILE_LOG = os.path.join(
    BACK_UP_PATH, '{}_file_info.txt'.format(COMBINED_FOLDER_NAMES))


BITSTREAM_LOG = os.path.join(
    BACK_UP_PATH, '{}_bitstreams.txt'.format(COMBINED_FOLDER_NAMES))

DUPLICATES_LOG = os.path.join(
    BACK_UP_PATH, '{}_duplicates.txt'.format(COMBINED_FOLDER_NAMES))


def main():
    if not os.path.isdir(BACK_UP_PATH):
        os.makedirs(BACK_UP_PATH)

    file_info = get_previous_file_info()

    all_files, bitstream_folders = get_all_files_and_bitstream_folders(
        TARGET_FOLDERS)

    if COMPRESSION_FLAG[0]:
        if COMPRESSION_FLAG[1].lower() == 'file':
            if os.path.isfile(ZSTD_EXE):
                print("Using zstd for individual bitstream file compression")
                compress_bitstream_files_zstd(bitstream_folders)
            else:
                print(
                    "Warning: zstd.exe not found in script directory, unable to proceed with file-by-file compression.")
        elif COMPRESSION_FLAG[1].lower() == 'folder':
            print("Using tarfile for entire folder compression of bitstreams")
            compress_bitstream_folders_tarfile(bitstream_folders)
        else:
            print("WARNING: Invalid compression method specified, please use 'File' or 'Folder'. Compression not proceeding.\n\n")

    unhashed_files = get_unhashed_files(all_files, file_info)
    if unhashed_files:
        print('Creating hashes for all unhashed files...')
        file_info.update(get_new_hashes(unhashed_files))

    print('Checking if any duplicates were found...')
    duplicates = find_duplicates(file_info)

    print('Deleting duplicate files...')
    duplicates, files_to_delete = get_duplicates_to_delete(duplicates, file_info)
    
    file_info = delete_files(files_to_delete, file_info) # Uncomment this to actually delete the identified files

    save_backup_files(bitstream_folders, file_info, duplicates)


def get_previous_file_info():
    file_info = dict()
    if os.path.isfile(INFO_FILE_LOG):
        print('Using previously found file hashes...')
        with open(INFO_FILE_LOG) as file:
            file_info_data = file.read()
        file_info = json.loads(file_info_data)
    else:
        print('No previous hashes found')
    return file_info


def get_all_files_and_bitstream_folders(target_folders):
    """returns a list of all files found in the target directory"""
    start_time = time.time()
    all_files = list()
    bitstream_folders = list()
    print("Starting search for duplicate files in {}".format(TARGET_FOLDERS))
    for target_folder in target_folders:
        for count, (root, dirs, files) in enumerate(os.walk(target_folder)):
            all_files.extend([os.path.join(root, file) for file in files])
            for directory in dirs:
                if "bitstream" in directory.lower() and "Compressed" not in directory:
                    folder_path = os.path.join(root, directory)
                    # print('Found bitstream directory {}'.format(folder_path))
                    bitstream_folders.append(folder_path)
            if count % 100 == 0:
                print('{} files scanned'.format(len(all_files)))
    print('File Scan: Time taken {} seconds'.format(time.time()-start_time))

    print("There are {} bitstream folders".format(len(bitstream_folders)))
    print('There are {} files in {}'.format(
        len(all_files), COMBINED_FOLDER_NAMES))
    return all_files, bitstream_folders


def compress_bitstream_folders_tarfile(bitstream_folders):
    start_time = time.time()
    for bitstream_folder in bitstream_folders:
        print('Compressing {}'.format(bitstream_folder))
        with tarfile.open("{}_Compressed.tar.bz2".format(bitstream_folder), "w:bz2") as tarhandle:
            for root, dirs, files in os.walk(bitstream_folder):
                for count, file in enumerate(files):
                    tarhandle.add(os.path.join(root, file), arcname=file)
                    if count % 1000 == 0:
                        print('{} files compressed'.format(count))
        # delete_bitstream_folder(bitstream_folder) Uncommenting this will delete bitstream folders after compression
    print('Bitstream Compression: Time taken {} seconds'.format(
        time.time()-start_time))


def compress_bitstream_files_zstd(bitstream_folders):
    start_time = time.time()
    for bitstream_folder in bitstream_folders:
        print("Compressing Bitstream Files in : {}".format(bitstream_folder))
        compressed_folder = bitstream_folder + "_Compressed"
        if not os.path.isdir(compressed_folder):
            os.mkdir(compressed_folder)
        for file in os.listdir(bitstream_folder):
            filepath = os.path.join(bitstream_folder, file)
            if os.path.isfile(filepath):
                cmd = "{} -19 -T0 --format=gzip {} -o {}.gz".format(
                    ZSTD_EXE, filepath, os.path.join(compressed_folder, file))
                pipe = subprocess.Popen(
                    cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                output, error = pipe.communicate()
                pipe.wait()
                # print("{} {}".format(output, error))
        # delete_bitstream_folder(bitstream_folder) Uncommenting this will delete bitstream folders after compression
    print('Bitstream Compression: Time taken {} seconds'.format(
        time.time()-start_time))


def get_unhashed_files(all_files, file_info):
    """Filters out the files that have already been hashed in a previous run"""
    start_time = time.time()
    unhashed_files = list()
    for count, file in enumerate(all_files):
        if file not in file_info:
            unhashed_files.append(file)
        if count % 100 == 0:
            print('Searching for previous hashes, {} files of {}'.format(
                count, len(all_files)))
    print('There are {} files to be hashed. Time taken {} seconds'.format(
        len(unhashed_files), (time.time()-start_time)))
    return unhashed_files


def get_new_hashes(list_of_files):
    """Returns a list of lists ie [hash,filesize,filename] for all files using multiprocessing"""
    start_time = time.time()
    pooler = multiprocessing.Pool(
        processes=multiprocessing.cpu_count(), maxtasksperchild=30)
    hashed_files = list()

    chunk_size = len(list_of_files) // multiprocessing.cpu_count()
    if chunk_size == 0:
        chunk_size = 1
        # prevent error if list_of_files is too short
    for file in tqdm.tqdm(pooler.imap_unordered(compute_hash, list_of_files, chunk_size),
                          total=len(list_of_files), miniters=100):
        hashed_files.append(file)

    pooler.close()
    pooler.join()
    new_file_info = dict()

    for hashed_file in hashed_files:
        if hashed_file[0] is not None:
            file = hashed_file[0]
            new_file_info[file] = dict()
            new_file_info[file]['hash'] = hashed_file[1]
            new_file_info[file]['size'] = hashed_file[2]
            new_file_info[file]['is_hard_link'] = False
    end_time = time.time()
    total_time = end_time - start_time
    print('File Hashing: Time taken {} seconds'.format(total_time))
    return new_file_info


def find_duplicates(file_info):
    """Takes the list of all file hashes and filters it to only those with duplicates"""
    start_time = time.time()
    duplicates = dict()

    for count, file in enumerate(file_info):
        md5 = file_info[file]['hash']
        size = file_info[file]['size']
        if os.path.isfile(file):
            if md5 not in duplicates:
                duplicates[md5] = [size, file]
            else:
                duplicates[md5].append(file)
        else:
            del file_info[file]
        if count % 100 == 0:
            print('{} files sorted'.format(len(duplicates)))

    duplicate_count = 0
    for md5 in list(duplicates):
        if len(duplicates[md5]) == 2:
            del duplicates[md5]
        duplicate_count += 1
        if duplicate_count % 100 == 0:
            print('{} duplicates found'.format(duplicate_count))
    print('Duplicate Search: Time taken {} seconds'.format(time.time()-start_time))
    return duplicates


def get_duplicates_to_delete(duplicate_dict, file_info):
    """Creates a dictionary of a base file to keep and the duplicates to delete"""
    files_to_delete = dict()
    for file_hash in duplicate_dict:
        duplicate_list = duplicate_dict[file_hash].copy()
        file_to_keep = duplicate_list[1]
        files_to_delete[file_to_keep] = list()
        for index, duplicate in enumerate(duplicate_list[2:]):
            # delete all files except for the first one
            try:
                if not file_info[duplicate]["is_hard_link"]:
                    files_to_delete[file_to_keep].append(duplicate)
            except KeyError:
                files_to_delete[file_to_keep].append(duplicate)
            duplicate_dict[file_hash][index +
                                      2] = duplicate_list[index + 2] + "_HARDLINK"
    return duplicate_dict, files_to_delete


def delete_bitstream_folder(folder_path):
    if os.path.isdir(folder_path):
        print("Deleting bitstream folder {}".format(folder_path))
        shutil.rmtree(folder_path, ignore_errors=False)


def delete_files(files_to_delete, file_info):
    for base_file, duplicate_files in files_to_delete.items():
        if os.path.isfile(base_file):
            for duplicate_file in duplicate_files:
                file_info = create_hardlinks(duplicate_file, base_file, file_info)
                if os.path.isfile(duplicate_file + '_HARDLINK'):
                    #print('Hardlink successfully created, deleting duplicate and renaming hardlink')
                    try:
                        os.remove(duplicate_file)
                        os.rename(duplicate_file + '_HARDLINK',duplicate_file)
                        file_info[duplicate_file]['is_hard_link'] = True
                    except Exception as error:
                        print('Failed to delete file {} due to {}'.format(duplicate_file, error))
                else:
                    print('Hardlink not created, keeping original file')
    return file_info


def create_hardlinks(duplicate_file, base_file, file_info):
    cmd = r'fsutil hardlink create "{}_HARDLINK" "{}"'.format(duplicate_file, base_file)
    pipe = subprocess.Popen(
        cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()
    pipe.wait()
    print("{} {}".format(output, error))
    return file_info


def save_backup_files(bitstream_folders, file_info, duplicates):
    with open(BITSTREAM_LOG, 'w', encoding="utf-8") as file:
        file.write('\n'.join(bitstream_folders))
    with open(INFO_FILE_LOG, 'w', encoding="utf-8") as file:
        file.write(json.dumps(file_info, indent=1))

    with open(DUPLICATES_LOG, 'w', encoding="utf-8") as file:
        file.write(
            '"FileSize in GB":\n\t"Locations of all copies of the file"\n')
        sorted_duplicates = sorted(
            list(duplicates.values()), key=lambda x: x[0], reverse=True)

        for sorted_duplicate in sorted_duplicates:
            file.write("\n")
            file_size_string = str(sorted_duplicate[0]) + ":\n\t"
            file.write(file_size_string)
            for file_path in sorted_duplicate[1:]:
                file.write('{}\n\t'.format(file_path))

        print('There are {} files with duplicates'.format(len(duplicates)))


def compute_hash(file_name):
    """Computes the hash and size for the given file"""
    hash_md5 = hashlib.md5()
    try:
        stat_info = os.stat(file_name)
        file_size = stat_info.st_size
        file_size = round(float(file_size/(1024*1024*1024)), 6)

        with open(file_name, "rb") as file:
            for chunk in iter(lambda: file.read(65536), b""):
                hash_md5.update(chunk)
    except (FileNotFoundError, PermissionError):
        return [None, None, None]
    return [file_name, hash_md5.hexdigest(), file_size]


if __name__ == '__main__':
    main()
