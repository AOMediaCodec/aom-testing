# Generate

## Test Settings

This generate script aims to simplify usage and improve maintainability. There are now only a few options that need to be specified for any given test. Within the test settings, there are two parts, 

### Test Parameters: Settings specific to encodings

**test_config**: Specify keyname of the desired test. These key names are selected from TEST_REPOSITORY dictionary 

**stream_dir**: The target folder with the clips of interest

**presets**: A list of presets to be tested. For encoders such as vvenc, the preset number will be converted to their relevant setting.

**intraperiod**: A value of -1 will ensure that no intraperiods are inserted by either specifying the number as +1 of the total number of frames or as -1 for supported encoders.

**decode_cycles**: Add a decoding process to allow the user to collect speed results with reference to the decoding process.

### System Parameters: Settings specific to the machine resource usage

**decode_threads**: Number of ffmpeg threads to use for decoding

**decode_jobs**: Number of processing cores to use for decoding

**num_pool**: Number of processing cores to use for encoding

**ffmpeg_job_count**: Number of cores to use for multithreaded ffmpeg operations such as downscaling 

**vmaf_job_count**: Number of cores to use for single threaded metric extractions

## Test Repository

The test repository is where all test configurations are located. A test configuration is composed of several parameters. 

**RC_VALUES**: Rate control values for CRF, CBR, or VBR type tests. Take the form of either Quantization Parameters (CRF/CBR) or Target Bitrates (VBR).

**RESOLUTION**: Provide the option to scale source clips and then encode both the source and scaled resolutions 

**DOWNSCALE_COMMAND**: The scaling methods used to scale to the aformentioned resolutions

**METRIC_COMMAND**: Once the encodings have finished, provides the compare the encoded bitstream to the source clip in order to extract quality metrics (PSNR, SSIM, VMAF, VMAF-NEG)

**ENCODE_COMMAND**: Specifies which encoder command lines to use. The encoder type, Rate Control Modes, number of passes, are all specified within a given command. 

Each of these parameters have an associated dictionary where a variety of pre defined values, commands can be added to or selected from.

A sample configuration for SPIE2021 svt would be added to the test repository in this way,      

'SPIE2021_svt'       : [RC_VALUES[List], RESOLUTION[List],   DOWNSCALE_COMMAND[String], METRIC_COMMAND[String], ENCODE_COMMAND[String]],

With the test name being the dictionary key, and each value in the item list being the targetted key in the dictionary list.

If the user does not want any scaling options, they would leave the resolution item blank with any negative variable such as, "False, None, 0".

'SPIE2021_svt'       : [RC_VALUES[List], None,   DOWNSCALE_COMMAND[String], METRIC_COMMAND[String], ENCODE_COMMAND[String]],


## Adding new Encode or Metric Commands

Adding new commands is simpler than ever. The user would take any given sample command and replace the changing variable with their associated string format.

#### Core Tokens

**Presets/Enc Modes**: {preset}

**RC Values**: {rc_value} 

**intraperiods/Keyints**: {intraperiod} 

**Source Clip**:  {clip} 

**Output Name**: {output_filename}

#### Tokens for clips without meta data

**Source Width**: {width} 

**Source Height**: {height} 

**Source Bitdepth**: {bitdepth} 

**Pixel Format**: {pixfmt}

**Source Frame per second Fractional format**: Numberator: {fps_num} | Denominator: {fps_denom}

**Source Frames per Second whole number format**: -r {fps}

#### Rescale Tokens

**Original source clip Width**: {ref_width}

**Original source clip Height**: {ref_height}

These tokens will be formatted with the values specified in the test configuration.

# Collection

The Collection process improved usability by removing all necessary options for collecting. There's no need to specify which encoder or metrics_computation you'd like to collect. Rather, any metrics that are detected will be collected. If scaling was detected, than all possible Y quality metrics will have their convex hulls collect in which the user can use or ignore them at their discretion. The number of convex hull points are auto selected depending on how many QPs are detected and are easily adjusted at the user's discretion. 

These metrics are collected through the idea of patterns. Each desired metric to be collected has their own unique pattern. These patterns are searched in all relevant encoding and metric logs and if they are found, they are added to the results. 

In the case where some encodings may have failed, an error will be outputted in the terminal aswell as having that value in the results file being replaced with "error_**metric**" as to alert the user. 

If the user needs to add new metrics, its as simple as adding their metric name to the "line_template" string at the begging of the script and then adding a new entry to the patterns dictionary with the appropriate pattern. 

# Excel Comparison

External Dependency: 

XlWings: 
Step to install
1. python -m pip install xlwings

Once the user has generate the result files using the collect script. They can create a "results" folder in the same directory as the comparison_templace.xlsm and ExcelCompare Script. If the user changes the name of the excel file, they should be sure to update the name in the python script **wb = xw.Book('File_Name.xlsm')**

Once the user has the desired results files in the results folder, they can run the script and the excel sheet will open, insert the results, and offer the user the following selections.

(1) : n0,n1,n2... vs n0 
(2) : n0,n1,n2... vs n0,n1,n2...

Option one allows the select a single encoder preset to compare against. For example, if we want to compare multiple presets against AOM M0, option 1 would be the correct selection.

Option two allows for a preset by preset comparison. For example, if we have two revisions of SVT with presets M0-M12 and we wanted to compare M0 vs M0, M1 vs M1, M2 vs M2, etc., we would select option 2.

After selecting the desired option, the list of pairs will be presented. Note, the encoder configuration appearing on the left hand side is designated as the reference encoder. 

If the results contained any convex hull files (PSNR, SSIM, VMAF, or VMAF-NEG) for both encoder being compare, an additional option will be presented.

__The option to do CVH comparisons are available__

__Select comparison Type__

__(1): Face to Face__

__(2): Convex Hull__

Selecting option 1 will ignore the convex hull results and only compare the quality metrics in the core results file.

Selection option 2 will use the points selected by the convex hull in for the BDR comparison.

At this point, all selections have been made and the comparisons will be performed.

**[NOTE], DO NOT ATTEMPT TO CLICK INTO OR USE EXCEL DURING THIS PROCESS**

Doing so could affect the accuracy of the results generated.

The comparison results will be added to the summary sheet for viewing.

# Bonus Features

## Command integrity Check in Generate

The generate script posseses the ability to detect changes that are made to the pre-existing test configurations. This is to guard against unintended changes to core script functions or command templates. If changes are detected, the user is alerted to which configurations have been affected and offers them the option to overwrite the pre-existing test setup. It's normally not recommended to change the pre-existing setups as they have been tested and verified for correct functioning. Upon adding a new test however, the user will be promped to overwrite which is perfectly acceptable.

## Version Checks

At the start of each run, each script (generate, collect, excel compare) will check with the latest version on gitlab to see if there are any fundamental changes that have been made. If there are difference between core sections of the code, the user will be warned that they are using a potentially out of date script that may contain unresolved bugs. This is to ensure that all users are using the most up to date scripts.


