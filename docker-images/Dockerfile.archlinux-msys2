FROM archlinux/archlinux:base-devel AS builder

ARG CCACHE_DIR=/ccache/
ENV PATH=/usr/lib/ccache/bin:$PATH

# Allows the `nobody` user to use sudo without a password
COPY <<EOF /etc/sudoers.d/nobody
nobody ALL = NOPASSWD: /usr/bin/pacman, /usr/bin/pacman-key
EOF

# Changes the default -j from 2.
# Enables ccache since compiling wine is slow and can help with future runs.
# Disables FORTIFY_SOURCE since wine doesn't like it.
COPY <<makeflags.conf <<ccache.conf <<ldflags.conf <<nofortify.conf /etc/makepkg.conf.d/
#!/hint/bash
MAKEFLAGS="-j$(($(nproc)+2))"
makeflags.conf
#!/hint/bash
BUILDENV=("\${BUILDENV[@]/!ccache/ccache}")
ccache.conf
#!/hint/bash
LDFLAGS="\${LDFLAGS} -pipe"
ldflags.conf
#!/hint/bash
CFLAGS="\${CFLAGS/-Wp,-D_FORTIFY_SOURCE=[0-3]/} -D__USE_MINGW_ANSI_STDIO=1"
CXXFLAGS="\${CXXFLAGS/-Wp,-D_FORTIFY_SOURCE=[0-3]/} -D__USE_MINGW_ANSI_STDIO=1"
nofortify.conf

ARG WINE_GIT_FOLDER=/wine
ADD --keep-git-dir=true --chown=nobody:nobody \
    https://gitlab.winehq.org/1480c1/wine.git#msys2-hacks-17 \
    ${WINE_GIT_FOLDER}

COPY --chown=nobody:nobody <<PKGBUILD <<30-win32-aliases.conf /wine-git/
# Trimmed down wine for only 64-bit and console primarily for CI
pkgname=wine-git
pkgver=9.9.r71.g1bccb4cf9a5
pkgrel=1
pkgdesc='A compatibility layer for running Windows programs (git version)'
arch=('x86_64')
url='https://www.winehq.org/'
license=('LGPL-2.1-or-later')
depends=(
    'fontconfig'
    'freetype2'
    'gcc-libs'
    'gettext'
    'libpcap'
    'libunwind'
)
makedepends=('git' 'perl' 'mingw-w64-gcc'
    'gnutls'
    'krb5'
    'libwbclient'
)
optdepends=(
    'gnutls'
    'krb5'
    'libwbclient'
)
options=('!staticlibs' '!lto')
provides=("wine=\${pkgver}")
conflicts=('wine')
source=("git+file://${WINE_GIT_FOLDER}"
        '30-win32-aliases.conf')
sha256sums=('SKIP'
            '9901a5ee619f24662b241672a7358364617227937d5f6d3126f70528ee5111e7')

prepare() {
    rm -rf build-64
    mkdir -p build-64
}

pkgver() {
    git -C wine describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g;s/^wine.//;s/^v//;s/\.rc/rc/'
}

build() {
    export CFLAGS+=' -ffat-lto-objects'

    # apply flags for cross-compilation
    export CROSSCFLAGS="\${CFLAGS/-Werror=format-security/}"
    export CROSSCXXFLAGS="\${CXXFLAGS/-Werror=format-security/}"
    export CROSSLDFLAGS="\${LDFLAGS//-Wl,-z*([^[:space:]])/}"

    # build wine 64-bit
    printf '%s\n' '  -> Building wine-64...'
    cd build-64
    ../wine/configure \\
        --prefix='/usr' \\
        --enable-win64 \\
        --enable-archs='x86_64' \\
        --disable-tests \\
        --with-mingw \\
        --without-x
    make
}

package() {
    # package wine 64-bit
    printf '%s\n' '  -> Packaging wine-64...'
    cd "\${srcdir}/build-64"
    DESTDIR=\${pkgdir} make install

    # Strips the dll files as options=(strip) only works for linux shared libraries
    if check_option "strip" "y"; then
        for file in \${pkgdir}/usr/lib/wine/x86_64-windows/*.dll; do
            echo "Stripping \${file}"
            x86_64-w64-mingw32-strip \${STRIP_SHARED} \${file}
        done
        for file in \${pkgdir}/usr/lib/wine/x86_64-windows/*.exe; do
            echo "Stripping \${file}"
            x86_64-w64-mingw32-strip \${STRIP_BINARIES} \${file}
        done
    fi

    # font aliasing settings for win32 applications
    install -d -m755 "\${pkgdir}/usr/share/fontconfig/conf.default"
    install -D -m644 "\${srcdir}/30-win32-aliases.conf" -t "\${pkgdir}/usr/share/fontconfig/conf.avail"
    ln -s ../conf.avail/30-win32-aliases.conf "\${pkgdir}/usr/share/fontconfig/conf.default/30-win32-aliases.conf"
}
PKGBUILD
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <alias binding="same">
    <family>MS Shell Dlg</family>
    <accept><family>Microsoft Sans Serif</family></accept>
    <default><family>sans-serif</family></default>
  </alias>
  <alias binding="same">
    <family>MS Shell Dlg 2</family>
    <accept><family>Tahoma</family></accept>
    <default><family>sans-serif</family></default>
  </alias>

  <alias binding="same">
    <family>MS Sans Serif</family>
    <prefer><family>Microsoft Sans Serif</family></prefer>
    <default><family>sans-serif</family></default>
  </alias>
</fontconfig>
30-win32-aliases.conf
WORKDIR /wine-git

# Set up ccache with 65534 is the nobody user
RUN --mount=type=cache,target=/var/cache/pacman/pkg/,sharing=locked \
    --mount=type=cache,target=/var/lib/pacman/sync/,sharing=locked \
    --mount=type=cache,target=${CCACHE_DIR},mode=755,uid=65534,gid=65534 \
    <<EOF
set -e
pacman-key --init
pacman-key --populate archlinux
pacman -Syuu --noconfirm --needed --overwrite '*' ccache
# Setup ccache wrappers, ccache doesnt need to be locked since it can handle multiple instances
runuser -unobody -- ccache -M 10G
ccache=$(command -v ccache)
for arch in i686 x86_64; do
    for compiler in c++ cc g++ gcc; do
        c=/usr/lib/ccache/bin/${arch}-w64-mingw32-${compiler}
        [[ -f $c ]] || ln -s "$ccache" ${c}
    done
done
# Reset account expiration for nobody
chage -E -1 nobody
# Build the package
runuser -unobody -- makepkg -s --noconfirm --needed
runuser -unobody -- ccache -s > /wine-git.log
rm -f /wine-git/wine-git-debug-*.pkg.tar.zst
EOF

FROM scratch AS msys2-installer

ADD https://github.com/msys2/msys2-installer/releases/download/nightly-x86_64/msys2-base-x86_64-latest.sfx.exe \
    /msys2-installer.exe

FROM archlinux/archlinux:base AS final

ENV LANG=en_US.UTF-8 \
    TZ=UTC
# Update and install some packages and setup wine
RUN --mount=type=cache,target=/var/cache/pacman/pkg/,sharing=locked \
    --mount=type=cache,target=/var/lib/pacman/sync/,sharing=locked \
    --mount=type=cache,target=/etc/pacman.d/gnupg/,sharing=locked \
    --mount=type=bind,from=builder,source=/wine-git/,target=/wine-git/ \
    <<EOF
set -eo pipefail
# Set some additional NoExtract options to save space from files we dont need
cat <<'FOE' >> /etc/pacman.conf
NoExtract  = usr/include/*
FOE
pacman-key --init
pacman-key --populate archlinux
pacman -Syuu --noconfirm --needed --overwrite '*' expac
pacman -U --noconfirm --overwrite '*' /wine-git/wine-git-*.pkg.tar.zst
expac -Qs '%o' '^wine-git$' | tr ' ' '\n' | pacman -S --asdep --needed --noconfirm -
pacman -Rs --noconfirm --nodeps expac
ldconfig
wine64 wineboot --init
wine64 reg ADD 'HKCU\Software\Wine\Drivers' /v Graphics /t REG_SZ /d 'null' /f
while pgrep wineserver > /dev/null; do sleep 1; done
EOF

RUN --mount=type=bind,from=msys2-installer,source=/msys2-installer.exe,target=/msys2-installer.exe <<EOF
set -eo pipefail
wine64 /msys2-installer.exe -o/root/.wine/drive_c/
sed -i 's;\[ -x /usr/bin/clear ];false;' /root/.wine/drive_c/msys64/etc/bash.bash_logout
EOF
RUN [ "/usr/bin/wine64", "C:/msys64/usr/bin/bash.exe", "-l", "-c", "exit" ]
SHELL [ "/usr/bin/wine64", "C:/msys64/usr/bin/bash.exe", "-l", "-c" ]
CMD [ "/usr/bin/wine64", "C:/msys64/usr/bin/bash.exe", "-l" ]
WORKDIR /root/.wine/drive_c/
ARG MSYS2_ROOT=/root/.wine/drive_c/msys64
RUN --mount=type=cache,target=${MSYS2_ROOT}/var/cache/pacman/pkg/,sharing=locked \
    --mount=type=cache,target=${MSYS2_ROOT}/var/lib/pacman/sync/ \
    <<EOF
set -eo pipefail
pacman -Syuu --noconfirm --needed \\
    mingw-w64-x86_64-ccache \\
    mingw-w64-x86_64-cmake \\
    mingw-w64-x86_64-cppcheck \\
    mingw-w64-x86_64-curl \\
    mingw-w64-x86_64-gcc \\
    mingw-w64-x86_64-meson \\
    mingw-w64-x86_64-nasm \\
    mingw-w64-x86_64-ninja \\
    mingw-w64-x86_64-pkgconf \\
    mingw-w64-x86_64-yasm \\
    mingw-w64-x86_64-zstd \\
    diffutils \\
    git \\
    make \\
    parallel \\
    vim
rm -rf /msys64/usr/bin/cmd
git config --global safe.directory '*'
echo 'will cite' | parallel --citation
EOF

ENV WINEDEBUG=-all \
    GIT_TERMINAL_PROMPT=0 \
    MSYSTEM=MINGW64 \
    CHERE_INVOKING=1
