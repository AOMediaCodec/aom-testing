FROM ubuntu:24.04

ENV DEBIAN_FRONTEND=noninteractive \
    GIT_TERMINAL_PROMPT=0 \
    PATH=/usr/lib/ccache:$PATH

COPY video.tar.zst \
    test-files/park_joy_90p_10_420.y4m.zst \
    test-files/screendata.y4m.zst \
    test-files/kirland_640_480_30.yuv.zst \
    test-files/akiyo_cif.y4m.zst \
    test-files/Chimera-Aerial_480x264_2997fps_10bit_420_150frames.y4m.zst \
    test-files/niklas_640_480_30.yuv.zst \
    test-files/park_joy_90p_8_420.y4m.zst \
    /

ARG DATE=1

RUN apt-get update && \
    apt-get install -yyy \
    apt-transport-https \
    bc \
    ca-certificates \
    curl \
    gnupg \
    jq \
    pkg-config \
    software-properties-common \
    zstd \
    python3-jinja2 \
    gcovr \
    python3-mesonpy \
    && \
    curl -L https://apt.kitware.com/keys/kitware-archive-latest.asc https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - && \
    apt-add-repository -y 'deb https://apt.kitware.com/ubuntu/ jammy main' && \
    apt-add-repository -y 'deb http://apt.llvm.org/jammy/ llvm-toolchain-jammy main' && \
    add-apt-repository -y ppa:ubuntu-toolchain-r/test && \
    apt-get update && \
    apt-get install -yyy \
    ccache \
    clang-15 \
    clang-tools-15 \
    clang-format-15 \
    clang-format \
    cmake \
    cmake-format \
    cppcheck \
    gcc-aarch64-linux-gnu g++-aarch64-linux-gnu \
    gcc-powerpc64le-linux-gnu g++-powerpc64le-linux-gnu \
    gcc-s390x-linux-gnu g++-s390x-linux-gnu \
    gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf \
    gcc-mingw-w64 g++-mingw-w64 \
    gcc-13-aarch64-linux-gnu g++-13-aarch64-linux-gnu \
    g++-13-powerpc64le-linux-gnu gcc-13-powerpc64le-linux-gnu \ 
    gcc-13-s390x-linux-gnu g++-13-s390x-linux-gnu \
    gcc-13-arm-linux-gnueabihf g++-13-arm-linux-gnueabihf \
    gcc-13 g++-13 \
    gcc g++ \
    git \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-good \
    gstreamer1.0-tools \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    make \
    nasm \
    ninja-build \
    valgrind \
    yasm \
    graphviz \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN \
    ln -s ../../bin/ccache /usr/lib/ccache/clang && \
    ln -s ../../bin/ccache /usr/lib/ccache/clang++ && \
    update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-13 20 && \
    update-alternatives --install /usr/bin/aarch64-linux-gnu-gcc aarch64-linux-gnu-gcc /usr/bin/aarch64-linux-gnu-gcc-13 20 && \
    update-alternatives --install /usr/bin/powerpc64le-linux-gnu-gcc powerpc64le-linux-gnu-gcc /usr/bin/powerpc64le-linux-gnu-gcc-13 20 && \
    update-alternatives --install /usr/bin/s390x-linux-gnu-gcc s390x-linux-gnu-gcc /usr/bin/s390x-linux-gnu-gcc-13 20 && \
    update-alternatives --install /usr/bin/arm-linux-gnueabihf-gcc arm-linux-gnueabihf-gcc /usr/bin/arm-linux-gnueabihf-gcc-13 20 && \
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13 20 && \
    update-alternatives --install /usr/bin/aarch64-linux-gnu-g++ aarch64-linux-gnu-g++ /usr/bin/aarch64-linux-gnu-g++-13 20 && \
    update-alternatives --install /usr/bin/powerpc64le-linux-gnu-g++ powerpc64le-linux-gnu-g++ /usr/bin/powerpc64le-linux-gnu-g++-13 20 && \
    update-alternatives --install /usr/bin/s390x-linux-gnu-g++ s390x-linux-gnu-g++ /usr/bin/s390x-linux-gnu-g++-13 20 && \
    update-alternatives --install /usr/bin/arm-linux-gnueabihf-g++ arm-linux-gnueabihf-g++ /usr/bin/arm-linux-gnueabihf-g++-13 20 && \
    update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-15 20 && \
    update-alternatives --install /usr/bin/clang clang /usr/bin/clang-15 20  && \
    update-alternatives --install /usr/bin/clang-format clang-format /usr/bin/clang-format-15 20

RUN \
    DIR=/tmp/doxygen && \
    DOXYGEN_VERSION=1.9.3 && \
    DOXYGEN_TAR=doxygen-${DOXYGEN_VERSION}.linux.bin.tar.gz && \
    DOXYGEN_URL=https://sourceforge.net/projects/doxygen/files/rel-${DOXYGEN_VERSION} && \
    DOXYGEN_SUM=e4db0a99e4f078ba4d8a590b6e3f6fdc2ff9207c50b1308a072be965e70f4141 && \
    mkdir -p ${DIR} && \
    cd ${DIR} && \
    curl -LO ${DOXYGEN_URL}/${DOXYGEN_TAR} && \
    printf '%s  %s\n' ${DOXYGEN_SUM} ${DOXYGEN_TAR} > sha256sum.txt && \
    sha256sum -c sha256sum.txt  && \
    tar -xf ${DOXYGEN_TAR}  && \
    cd doxygen-${DOXYGEN_VERSION}  && \
    make install
